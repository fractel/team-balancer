# Team Balancer
Team Balancer is a console application which distributes the right tasks for every user, so no one will stay 
without possible work. Users will get only tasks that they could deal with their level(points).

### Requirements ####
- python 3.5

### Algorithm ###

1. Team Balancer gets a dict with users and tasks data from json file.
    
2. Sort both dicts (users, tasks) in reversed mode (from bigger points to lower points).
    
3. Add tasks to the `complex_tasks` value if their points are bigger than an user with max points.
       
4. Check if tasks and users dicts have at least 1 user and 1 task.
     
5. Start a loop which will work while any task can be assigned to user. 
    
6. Go through every task (from heavy to easy) and check users (from senior to junior user level). Also skip tasks, which are in `complex_tasks` value.
       
7. If user can afford task (user have enough points for task points), then user points will be decreased by task points.
       
8. When user was assigned by some task he wouldn't join to assigning task loop again. This needs for an effectively distributing a task at first time. So no one wil stay without task if task can be assigned.
       
9. Team balancer checks if any user can let one of remain tasks. Quit loop if not. 
       
10. After first loop has ended then data will be unsorted, but TB won't sort it again. He would start to assign tasks again while condition (step 5) says True. So now dict with users has scattered items which TB would fill with remain tasks.
            
    
##### Advantages #####

- TB always assigns tasks to user if users have enough points to afford it.
- Everyone will get a most heavily task, so users` efficiency (% of employment) would be max in use.
- Have a good perfomance as TB doesn't sort dicts everytime joining the loop.
- If difference between tasks and users are not so big (even for a big number of tasks and users), it would have good speed.

##### Disadvantages #####

- Need to fully sort both dicts first time.
- If you have a big difference between task and user numbers (100 000 tasks, 20 000 users) it can show a slow perfomance on tasks (for about 13 sec.)

##### Notes #####

- Tasks are always starting from heavily to easier first time, TB doesn't do it in another way, because user efficiency could be more less in that way then are.
- Don't build on asyncio module as perfomance differnce of loop is not so big and project archirecture is in many times complicated.

##### For more details, see app/algorithm.py

### Configuration
You don't need to do any preparations.

### Logging
Team Balancer will create a `tb-output.txt` file in main folder and save logging info to him. You can always change a file to save logs by key `-o <your_file>`.

### Deployment instructions

Firstly, run interactive manager to save/remove/show tasks or users. It helps to not work with raw json.

Team Balancer has already created data example in `tb-info.json`. But anyway you can overwrite or delete them by 
working with a raw json.

So let's start, key `-i` start interactive manager and key `-a` will skip a building scheduler for users.

    python3.5 run.py -i -a

As an example, let's run Interactive Manager and add a user `Rachel` with `80` points:
    
	python3 run.py -i -a 
	--------------------------------------------------

	Interactive Manager
	Please choose what task do you want to manage.

	1. Tasks
	2. Users
	3. Exit

	Number [3]: 2
	--------------------------------------------------

	Interactive Manager
	Please choose action for key `users`:

	1. Add
	2. Remove
	3. Show
	4. Exit

	Number [4]: 1
	--------------------------------------------------

	Name of item: Rachel 
	His points: 80

	Item `Rachel` in key `users` successfully created

	--------------------------------------------------

	Interactive Manager
	Please choose action for key `users`:

	1. Add
	2. Remove
	3. Show
	4. Exit

	Number [4]: 
	--------------------------------------------------


	New changes have been saved successfully.


	Interactive Manager
	Please choose what task do you want to manage.

	1. Tasks
	2. Users
	3. Exit

	Number [3]: 
	--------------------------------------------------

	All done. Exiting.

    
In case if you have some mistakes, Interactive Manager will help you
 
	Interactive Manager
	Please choose action for key `users`:

	1. Add
	2. Remove
	3. Show
	4. Exit

	Number [4]: 1
	--------------------------------------------------

	Name of item: Name with wrong points
	His points: 0 or -5

	Points should be digit and more than 0

	--------------------------------------------------


To show all users or tasks by Interactive manager, just enter an empty field in `show` action.
  
	Interactive Manager
	Please choose action for key `users`:

	1. Add
	2. Remove
	3. Show
	4. Exit

	Number [4]: 3
	Provide the item ID(username or task name): 
	--------------------------------------------------

	{
	    "Rachel": {
	        "efficiency": 0.75,
	        "points": 80,
	        "tasks": {
	            "Sixth task": 60
	        }
	    }
	}

	--------------------------------------------------


### Build a scheduler

After running this, TB will build a scheduler for users.

    python3.5 run.py
    
![picture of result](https://pbs.twimg.com/media/DA1qEFKWsAAM8yQ.jpg:large)   

If you want to avoid fully info about results, use key `-n` and it would hide info (just build and save results to file). 

    python3.5 run.py -n

### Result

File `tb-info.json` will be automatically updated after your scheduler has built plans for users.

    "users": {
        "Gregoriy": {
            "tasks": {
                "Fourth task": 31,
                "First task": 39
            },
            "points": 76,
            "efficiency": 0.9210526315789473
        },
        "Christ": {
            "tasks": {
                "Third Task": 23
            },
            "points": 25,
            "efficiency": 0.92
        },
        "Rachel": {
            "tasks": {
                "Second task": 56,
                "Eighth task": 22
            },
            "points": 80,
            "efficiency": 0.975
        },
        "John": {
            "tasks": {
                "Sixth task": 60,
                "Seventh Task": 29
            },
            "points": 89,
            "efficiency": 1.0
        }