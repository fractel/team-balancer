import logging


# interactive menu
DIG_MENU_CHOOSE_KEY = """
Interactive Manager
Please choose what task do you want to manage.

1. Tasks
2. Users
3. Exit

Number [3]: """

DIG_MENU_CHOOSE_ACTION = """
Interactive Manager
Please choose action for key `%s`:

1. Add
2. Remove
3. Show
4. Exit

Number [4]: """


SHOW_USER_PLANS = """
User: %s
        [
            efficiency: %.2f%%,
            tasks: %r
            all_points: %d
            remain_points: %d
        ]
"""

SHOW_CONCLUSION = """
Conclusion
    The min of remain task points left: %d
    The max of user points left: %d
    Remain tasks: %r
    Complex tasks: %r
    Assigned tasks: %r
"""

HORIZONTAL_LINE = '-' * 50

# Logging configuration details
LOGGING_CONFIGURATIONS = dict(
    format='%(asctime)s -> %(levelname)s :  %(message)s',
    level=logging.DEBUG
)

# Application files for logging output and data input.
TB_INPUT_FILENAME = 'tb-info.json'
TB_OUTPUT_FILENAME = 'tb-output.txt'
