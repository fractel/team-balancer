import argparse

from .constants import TB_OUTPUT_FILENAME


def get_parsed_args():
    """
    Gives parsed arguments from command line.

    :returns: argparse.Namespace with parsed arguments
    """
    parser = argparse.ArgumentParser(
        prog="Team Balancer",

        usage="Team Balancer (TB) is a console application for searching the "
              "right decision to choose and distribute tasks among users.",

        description="For more details about how Team Balancer "
                    "works, please visit a tutorial link: "
                    "https://bitbucket.org/maxups/team-balancer"
    )

    # ===============================
    # Arguments for parsing command line
    # ===============================

    parser.add_argument('-i', '--interactive-mode',
                        nargs='?',
                        type=bool,
                        const=True,
                        default=False,
                        help="This options tell TB to use interactive mode. "
                             "If you want to add/remove task or user more "
                             "easier, without writing your json data - use "
                             "it. It will run  help menu with instruction "
                             "for managing with `tb-info.json`."
                        )

    parser.add_argument('-a', '--avoid-tb',
                        nargs='?',
                        type=bool,
                        const=True,
                        default=False,
                        help="When use, Team Balancer will not plan a task "
                             "for users. For default, it's in use."
                        )

    parser.add_argument('-o', '--output',
                        nargs='?',
                        type=str,
                        default=TB_OUTPUT_FILENAME,
                        help="This option tells TB to logging into file, not "
                             "in stout. Option can take name and then create "
                             "a file with given name. If name wasn't given,"
                             "set filename as `tb-output.txt`.",
                        dest='output_file'
                        )

    parser.add_argument('-n', '--hide-details',
                        nargs='?',
                        type=bool,
                        const=False,
                        default=True,
                        help="This option tells TB to show detailed "
                             "information about what TeamBalancer did.",
                        dest='details'
                        )

    return parser.parse_args()
