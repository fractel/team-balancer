import json
import logging
import os

from app import constants as consts
from app.algorithm import TeamBalancer
from app.command_parser import get_parsed_args
from app.interact_manager import InteractiveMode, print_both_outputs, logger


def set_logging(filename=None):
    """Setting the logging configuration by `logging` module. After, you can
    use `logger` to write log info.
    If filename is provided it will open it and write to file log info.

    :param filename: str - the name of file for output
    :return: None
    """
    logging.basicConfig(filename=filename, **consts.LOGGING_CONFIGURATIONS)
    if filename is None:
        logger.info("Output is set to stdout.")
    else:
        logger.info("Output is set to `%s`.", filename)


def check_input_file():
    """
    If file exists, function will skip.
    Otherwise, creates empty file with `TB_INPUT_FILENAME` value and add
    empty json data.

    :return: None
    """
    if os.path.exists('tb-info.json'):
        logger.debug('Input file `%s` exists.', consts.TB_INPUT_FILENAME)
        return False

    # will create file if it doesn't exist.
    with open(consts.TB_INPUT_FILENAME, 'w') as fp:
        # adds to file empty tasks and users data with pretty intend in file
        json.dump(
            {'tasks': {}, 'users': {}}, fp, indent=True
        )
    logger.debug('Input file `%s` created.', consts.TB_INPUT_FILENAME)
    return True


def main():
    """
    The main function of application. It checks parsed arguments and executes
    particular actions given by them.

    So the function does:

        1. Gets parsed data;
        2. Set logging configs;
        3. Run a interactive mode;
        4. Start a building scheduler.

    :return: None
    """
    # parsing args from command line, will get flags and args.
    parsed_args = get_parsed_args()
    # setting the logging configuration with `parsed_args.output_file`, if
    # it's None, logging will stream to stdout, otherwise will print info
    # to file with filename
    set_logging(filename=parsed_args.output_file)
    # check input file, if it doesn't exist will create.
    check_input_file()

    if parsed_args.interactive_mode:
        # entering to interactive mode where user can manage (add/remove)
        # tasks or users data by simple steps. If `tb-info.json` have broken
        # JSON data, `run_interactive_mode` will exit the application.
        # Because if file is broken then nothing can be done further.
        InteractiveMode().run()

    if not parsed_args.avoid_tb:
        # will work if key -a/--avoid-tb was not provided.
        # Basically, it helps when someone needs to call only use
        # interactive manager without plans a tasks
        team = TeamBalancer()
        # building a plans for users, then show and save it to file.
        team.build_schedule(show_plan=parsed_args.details, save_to_file=True)

    print_both_outputs("All done. Exiting.", logging.INFO)
