import logging

from app.constants import TB_INPUT_FILENAME, SHOW_USER_PLANS, SHOW_CONCLUSION
from app.interact_manager import (
    get_json_from_file, save_dict_to_file,
    print_both_outputs, logger, ConsoleColor
)


class TeamBalancer(object):
    """
    Class provides a building scheduler algorithm. It helps to distribute
    the right tasks for every user, so no one will stay without work and
    users will take task that they could work with their level and complete.

    For more details about algorithm, see `build_schedule` and `_plan` funcs.
    """

    def __init__(self):
        """
        Parameters:
          - `data`: dict -> with keys `tasks` and `users`
          - `users`: list -> can include lists with [user_name, user_points]
                               in reversed sorted position by user_points
          - `tasks`: list -> can include lists with [task_name, task_points]
                               in reversed sorted position by task_points

          - `complex_tasks`: set -> set of complex tasks which will be skipped
          - `remain_tasks`: set -> set of remain tasks which wasn't
                                   assigned to anyone.
          - `applied_tasks`: set -> set of remain tasks which was applied
                                   to users.

          - `users_number`, `tasks_number`: int ->
                            the length of `users` and `tasks` respectively

          - `max_remain_user_points`, `min_remain_task_points`: int ->
                            count of max and min remain users and tasks
                            points respectively.

          - `maximum_user_points`, `minimum_task_points`: int ->
                            max and min user points that users
                            have respectively.

          - `loop_counter`: int -> counter for the number of loops used.
        """
        with open(TB_INPUT_FILENAME) as fp:
            self.data = get_json_from_file(fp)

        self.users = []
        self.tasks = []

        self.complex_tasks = set()
        self.remain_tasks = set()
        self.applied_tasks = set()

        self.users_number = 0
        self.tasks_number = 0

        self.max_remain_user_points = 0
        self.min_remain_task_points = 0

        self.maximum_user_points = 0
        self.minimum_task_points = 0

        self.loop_counter = 0

    def get_name_and_points_by_key(self, key):
        """
        Function returns a list of names and points a key.
        Key can only be `tasks` or `users`. As interactive manager saves
        to input file:

        ---------------------------
        {
            "tasks": {
                "First task": {
                    "points": 45
                }
            },
            "users": {
                "John": {
                    "points": 0,
                    "efficiency": 0,
                    "tasks": []
                }
            }
        }
        ---------------------------
        So we can be confident in calling a `self.data[key][name]['points']`
        As key is always `tasks` or `users` and `points` exists in both keys.


        Returning a list, not a tuple, because in further steps user points
        could be decreased.

        :param key: str -> can be `tasks` or `users`
        :return: list -> [name, points]
        """
        for name in self.data[key]:
            yield [name, self.data[key][name]['points']]

    def sort_data_by_points(self):
        """
        Function sorts a reversed data by points
        and saves it to keys (can only be `tasks` or `users`)

        :return: None
        """
        for key in ('users', 'tasks'):
            self.__dict__[key] = sorted(
                # yield [name, points] by particular key
                self.get_name_and_points_by_key(key),
                # will match a points
                key=lambda s: s[1],
                # need reversing for further handling
                reverse=True
            )

    def refresh_user_tasks(self):
        """
        Function cleans a users tasks, which could be set before.
        Basically it helps to avoid having the same name of task in
        individual user tasks.

        :return: None
        """
        users = self.data['users']
        for name in users:
            # removes all tasks from every user
            users[name]['tasks'].clear()
        logger.debug('Old users tasks was cleared.')

    def _set_lower_task_points(self):
        """
        Func sets the min value of task points to `self.minimum_task_points`

        :return: None
        """
        self.minimum_task_points = self.tasks[-1][1]
        logger.debug('Minimum tasks points is %d', self.minimum_task_points)

    def _set_upper_user_points(self):
        """
        Func sets the max value of task points to `self.maximum_user_points`

        :return: None
        """
        self.maximum_user_points = self.users[0][1]
        logger.debug('Maximum user points is %d', self.maximum_user_points)

    def _plan(self):
        """
        Function plans a users scheduler by picking up the right task based
        on points which users can let.

        Basically, it works this way: func starts a loop (where we have
        reversed sorted list of tasks) and try to find which task points are
        equal or less to user points. If it happens, then we remove this task
        from `self.remain_tasks`, and after this, task will no appear in user
        loop again (as it is applied to only this user).

        Firstly, func tries to apply the most difficult task to senior user.
        If it happens, this 'senior' will not appear again in user loop, while
        all other users will be checked to apply on subsequent tasks.

        Therefore, this function should be called how many times, until then
        the user with maximum points can allow the task with minimum points.

        :return: None
        """
        # `user_cursor` is a value for skipping users which have already got
        # a task, so we will return to them after again entering func `_plan`
        user_cursor = 0
        for tinx in range(self.tasks_number):
            task_name, task_points = self.tasks[tinx]
            if task_name not in self.remain_tasks:
                # will skip if this tasks are already applied to someone.
                continue

            for uinx in range(user_cursor, self.users_number):
                # will loop only users who have not got their tasks
                user_name, user_points = self.users[uinx]

                if self.loop_counter:
                    # Basically this needs only for the loop_counter > 0
                    # when `self.tasks` are not more reverse sorted, so
                    # we are trying to only fill remain points users with task
                    user_cursor += 1

                # condition works if user can get this task
                if task_points <= user_points:
                    # assign this task to the user
                    user_task_list = self.data['users'][user_name]['tasks']
                    user_task_list[task_name] = task_points

                    if not self.loop_counter:
                        # Basically this needs only for the loop_counter == 0
                        # when `self.tasks` are still reverse sorted
                        user_cursor += 1

                    # decrease a number of user points by task points
                    # and save it to further check as remain user points.
                    user_left_points = user_points - task_points
                    self.users[user_cursor - 1][1] = user_left_points

                    # task is assigned to user
                    # so we delete it from remain tasks
                    self.remain_tasks.remove(task_name)
                    break

    def _set_left_points(self):
        """
        Function checks min and max remain points of tasks and points
        respectively. It helps for determining users that
        can afford more task(s).
        If no tasks or remain user points left then function will quit and
        return False

        :return: bool -> True if some tasks can be assigned, otherwise False
        """
        if not self.remain_tasks:
            # exit if no task left to assign
            return False
        # need to reset the points, so we set both of them to 0
        self.min_remain_task_points = 0
        self.max_remain_user_points = 0
        for task_name, task_points in self.tasks:
            # find the remain task with min points.
            if task_name in self.remain_tasks:
                # this will work if task still doesn't assign to any user.
                if self.min_remain_task_points > task_points or \
                                self.min_remain_task_points == 0:
                    # ----------------------------------------------------
                    # this will work if `min_remain_task_points` is 0 or
                    # task have less points than `min_remain_task_points`
                    self.min_remain_task_points = task_points

        for user_name, remain_points in self.users:
            # find the user with max remain points.
            if remain_points > self.max_remain_user_points:
                self.max_remain_user_points = remain_points

        logger.debug(
            "Max remains users points are: %d", self.max_remain_user_points
        )
        logger.debug(
            "Min remain tasks points are: %d", self.min_remain_task_points
        )

        # if even one of them is 0, then we need to quit,
        # as no user or task left to work with.
        return self.max_remain_user_points and self.min_remain_task_points

    def _set_tasks_length(self):
        """
        Set tasks length to `tasks_number`.

        :return: None
        """
        self.tasks_number = len(self.tasks)
        logger.debug('Tasks was found: %d', self.tasks_number)

    def _set_user_length(self):
        """
        Set user length to `users_number` as it could change throw schedule
        handling and we would need to repeat this function.

        :return: None
        """
        self.users_number = len(self.users)
        logger.debug('Users was found: %d', self.users_number)

    def _prepare_configs(self):
        """
        Function calls all options which need to be set before `plan` will run

        :return: None
        """
        # Setting top and bottom of user and task points respectively
        self._set_lower_task_points()
        self._set_upper_user_points()
        # Setting the user and tasks, as it can have
        # arbitrary value across handling
        self._set_tasks_length()
        self._set_user_length()
        # should be set as max user points  or loop in `build_schedule`
        # won't work. This needs only before start
        self.max_remain_user_points = self.maximum_user_points

    def spare_complex_task(self):
        """
        Function removes from `self.tasks` tasks which are more complex for
        even a user with max points. So nobody can't pretend for this task.

        :return: None
        """
        # cursor for what point in `self.tasks` we need to stop,
        # as after this cursor the complex data will start
        complex_pos = 0
        for name, points in self.tasks:
            if points > self.maximum_user_points:
                # will do if task point are bigger
                # than the user with max points
                complex_pos += 1
                # saves a tasks which can't be solved
                self.complex_tasks.add(name)
            else:
                # add all names of tasks, further will delete resolved tasks
                self.remain_tasks.add(name)
        # removing complex tasks from `self.tasks`
        self.tasks = self.tasks[complex_pos:]
        # need to be set as breakpoint of loop,
        # in case when will be only one loop
        self.minimum_task_points = self.tasks[0][1]
        # resetting task length as it has been changed
        self._set_tasks_length()

    def build_schedule(self, show_plan=False, save_to_file=False):
        """
        Function provides a prepare data for further building a plan for
        every user based on possible tasks. Function spare complex tasks from
        loop as nobody can do them, also clean user tasks if they exist.
        After preparation done function starts a loop while the user with most
         remain points could have a task with min points.

        :param show_plan: if True, will print info about plans
        :param save_to_file: bool -> if True, will save to `TB_INPUT_FILENAME`
        :return: None
        """
        # Firstly, cleaning the older task in users
        self.refresh_user_tasks()
        # sorting 'tasks' and 'users' data by points.
        self.sort_data_by_points()
        if self.tasks and self.users:
            # it means that data has at least one task and one user
            # when we have data in `self.users` and `self.tasks` we
            # could set configs about the data of these dicts.
            self._prepare_configs()
            # setting the max and min points for
            # further avoiding unnecessary data
            self._set_lower_task_points()
            self._set_upper_user_points()
            # will spare tasks from `self.tasks` which are too
            # difficult for even a user with max points. Also add to
            self.spare_complex_task()
            # saving all tasks to applied tasks for further intersection
            self.applied_tasks.update(self.remain_tasks)

            while self.max_remain_user_points >= self.minimum_task_points:
                # will do while count of max user points is still bigger
                # than remain the lower points of task
                self._plan()
                # counting the number of loops for further logging
                self.loop_counter += 1
                if not self._set_left_points():
                    # this helps if both remain points are 0. This is very
                    # rare case, but it needs in case forever loop
                    break

            # Find a difference between all tasks and remain tasks,
            # so we will get an applied tasks
            self.applied_tasks -= self.remain_tasks
            self.log_debug_info()

        else:
            print_both_outputs(
                "Your file doesn't have enough data. Please, fill it.",
                logging.WARNING
            )
        if show_plan:
            # will show all details about remain task which are complex
            # or unresolved for users. Also, show a task which are applied.
            self.show_user_plans()

        if save_to_file:
            # saves to file all changes
            save_dict_to_file(self.data)

    def log_debug_info(self):
        """
        Log debug info to output logging file.
        """
        logger.info('Applied tasks: %d', len(self.applied_tasks))
        logger.info('Remain tasks: %d', len(self.remain_tasks))
        logger.info('Complex tasks: %d', len(self.complex_tasks))
        logger.info('Loops used: %d', self.loop_counter)

    def show_user_plans(self):
        """
        Show detailed information about scheduler (assigned, complex,
        remain tasks) of users which was build by `_plan`.

        :return: None
        """
        # `self.users` = [[user_name, remain_points],..]
        for user_name, remain_points in self.users:
            # get user link from global data
            user = self.data['users'][user_name]
            # calculate how is user is charged by work.
            efficiency = 1 - remain_points / user['points']
            # this need to set the efficiency to user
            self.set_users_efficiency(user_name, efficiency)

            # showing user details
            print(SHOW_USER_PLANS % (
                ConsoleColor.HEADER + user_name + ConsoleColor.WHITE,
                efficiency * 100,
                user['tasks'],
                user['points'],
                remain_points
            ))

        self.print_conclusion()

    def set_users_efficiency(self, user, eff):
        """
        Sets to global json data an efficiency work of user tasks.

        :param user: str -> user nickname
        :param eff: int -> % of efficiency work
        :return None
        """
        self.data['users'][user]['efficiency'] = eff

    def print_conclusion(self):
        """
        Function shows a conclusion of scheduler builder.
        
        :return None
        """
        print(SHOW_CONCLUSION % (
            self.minimum_task_points,
            self.max_remain_user_points,
            self.remain_tasks,
            self.complex_tasks,
            self.applied_tasks,
        ))
